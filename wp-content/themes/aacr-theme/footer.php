<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="footer-testimonials">
						<h2 class="text-center">What People Are Saying About Ann Aaron</h2>
						<?php echo do_shortcode('[elementor-template id="77"]'); ?>
						<div class="row">
							<div class="col-sm-2">
								<img src="/wp-content/uploads/2018/10/Icon_National_Women_In_Roofing-01.png">
							</div>
							<div class="col-sm-2">
								<img src="/wp-content/uploads/2018/10/Icon_Weatherford_Chamber_Commerce-01.png">
							</div>
							<div class="col-sm-2">
								<img src="/wp-content/uploads/2018/10/Icon_Grapevine_Chamber_Commerce-01.png">
							</div>
							<div class="col-sm-2">
								<img src="/wp-content/uploads/2018/10/Icon_Roofing_Contractors_Association_Texas-01.png">
							</div>
							<div class="col-sm-2">
								<img src="/wp-content/uploads/2018/10/Icon_BBB_Accredited_Business-01.png">
							</div>
							<div class="col-sm-2">
								<img src="/wp-content/uploads/2018/10/Icon_Ann_Aaron_Interior_Design-01.png">
							</div>
						</div>
					</div><!-- .site-info -->
					

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->
	<div class="footer-contact">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-4">
					<img src="/wp-content/uploads/2018/10/ann-aaron-logo.png" alt="Ann Aaron Roofing and Contracting">
				</div>
				<div class="col-sm-4">
					<h6 class="text-center">Sign Up for Our Newsletter</h6>
					<?php echo do_shortcode('[forminator_form id="86"]'); ?>
				</div>
				<div class="col-sm-4 contact">
					<h6>Contact</h6>
					<a href="tel:8175737663"><i class="fa fa-phone"></i> 817-573-ROOF (7663)</a><br><br>
					<a href="mailto:info@annaaron.com"><i class="fa fa-envelope"></i> info@annaaron.com</a><br><br>
					<p>200 S Oakridge Drive, STE 101-501
							Hudson Oaks, TX 76087</p>
				</div>
				<div class="col-sm-12 social">
					<div class="row justify-content-center">
						<div class="col-sm-2 text-center">
							<a href="https://www.facebook.com/annaaroncontractingandroofing/" target="_blank"><i class="fa fa-facebook"></i></a>
						</div>
						<div class="col-sm-2 text-center">
							<a href="https://www.instagram.com/annaaron2015/" target="_blank"><i class="fa fa-instagram"></i></a>
						</div>
						<div class="col-sm-2 text-center">
							<a href="https://twitter.com/Annaaron2015" target="_blank"><i class="fa fa-twitter"></i></a>
						</div>
						<div class="col-sm-2 text-center">
							<a href="https://www.linkedin.com/company/ann-aaron-interior-design-llc?trk=biz-companies-cym" target="_blank"><i class="fa fa-linkedin"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">© <?php echo date('Y'); ?> Ann Aaron Contracting and Roofing</div>
</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
</body>

</html>

