jQuery(document).ready(function($){
  var barTop = $('#bar-top');
  var barMidL = $('#bar-mid-l');
  var barMidR = $('#bar-mid-r');
  var barBottom = $('#bar-bottom');
  var circle = $('#circle');
  var menuIcon = $('#menu-icon');

  var tl = new TimelineLite({
  	paused:true,
  });
  var m = new TimelineLite({
  	paused:true,
  });

  TweenLite.defaultEase = Expo.easeInOut; // Change the default easing

  tl.to(barTop, 0.2, {y: 5,	})
  	.to(barBottom, 0.2, {y: -5, },0)
  	.to(barTop, 0.3, {rotation:45, y:0, x:10, transformOrigin:"left top", },0.15)
  	.to(barBottom, 0.3, {rotation:-45, y:0, x:10, transformOrigin:"left bottom", },0.15)
  	.to(barMidL, 0.4, { opacity:0, scaleX:0, transformOrigin:"left",}, 0)
  	.to(barMidR, 0.4, { opacity:0, scaleX:0, transformOrigin:"right", }, 0)
  	.to("#menu-icon path", 0.5, {fill: "#899FA9"}, 0.1)
  	.to("#menu-icon #circle", 0.5, {stroke: "#899FA9"}, 0.1)
  	.to(menuIcon, 0.5, {rotation:90, marginBottom: "-17", marginRight: "10", }, 0.1)
  	.fromTo(circle, 0.6, { opacity:0, scale: 0, transformOrigin: "50% 50%", ease: Bounce.easeOut }, {scale: 1, opacity: 1, ease: Elastic.easeOut.config(1, 0.8), }, 0.5)
  	.to([barTop, barBottom], 0.5, {scale: 0.5, transformOrigin: "50% 50%", ease: Power4.easeOut}, 0.5)
  	.to('#sidenav', 0.5, {right: 0, ease: Back.easeOut.config(1)}, "-=10s")
  $('#menu').click(function(){
  	if ( $(this).hasClass('toggled') ) {
  		tl.reverse();
  		m.reverse();
  	}
  	else {
  		tl.play();
  		m.play();
  	}
  	$(this).toggleClass('toggled');
  });
})
